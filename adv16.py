import unittest

def create_list(input):
    lst = []
    for i in range(len(input)):
        lst.append(int(input[i]))
    return lst

def get_outputs_part_b(lst):
    outputs = []
    first_sum = sum([int(x) for x in lst])
    outputs.append(first_sum % 10)
    for num in lst:
        first_sum -= num
        outputs.append(first_sum % 10)
    return outputs

def get_output(lst, output_index):
    base_pattern = [0, 1, 0, -1]
    repeated_seq = []
    for i in base_pattern:
        for j in range(output_index):
            repeated_seq.append(base_pattern[i])
    sum = 0
    for i in range(len(lst)):
        input_val = lst[i]
        j = (i + 1) % len(repeated_seq)
        multiplier = repeated_seq[j]
        #print(f"{input_val} * {multiplier} + ")
        val = input_val * multiplier
        sum += val
    return abs(sum) % 10

def get_phase_output(input):
    input_lst = create_list(input)
    output = []
    for i in range(1, len(input_lst) + 1):
        output.append(get_output(input_lst, i))
    return ''.join(map(str,output))

def get_phase_output_part_b(input):
    input_lst = create_list(input)
    outputs = get_outputs_part_b(input_lst)
    return ''.join(map(str, outputs))

def repeat_phases_part_b(input):
    phase_output = input
    for i in range(100):
        phase_output = get_phase_output_part_b(phase_output)
    return phase_output


def repeat_phases(input, times):
    phase_output = input
    for i in range(times):
        phase_output = get_phase_output(phase_output)
    return phase_output

def repeat_input(input, times):
    contentBuilder = ""
    for i in range(times):
        contentBuilder = contentBuilder + input
    return contentBuilder

class TestPhaser(unittest.TestCase):
    def test_small_program(self):
        input_list = "12345678"
        lst = create_list(input_list)
        self.assertEqual(get_output(lst, 1), 4)
        self.assertEqual(get_output(lst, 2), 8)
        self.assertEqual(get_output(lst, 3), 2)
        self.assertEqual(get_output(lst, 4), 2)

        self.assertEqual(get_phase_output(input_list), "48226158")
    def test_part_b(self):
        input_list = "03036732577212944063491565474664"
        input_offset = int(input_list[0:7])
        new_input = repeat_input(input_list, 10000)

        lel = repeat_phases_part_b(new_input[input_offset:])
        self.assertEqual(lel[:8], "84462026")

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPhaser)
    unittest.TextTestRunner(verbosity=2).run(suite)
    print("PART A")
    f = open("input16.txt", 'r')
    content = f.read()
    f.close()
    lel = repeat_phases(content, 100)
    print(lel[:8])

    print("PART B")
    input_list = content
    input_offset = int(content[0:7])
    new_input = repeat_input(input_list, 10000)
    lel = repeat_phases_part_b(new_input[input_offset:])
    print(lel[:8])