import re
import math

DEAL_INTO_NEW_STACK = 0
DEAL_WITH_INCREMENT = 1
CUT = 2

def parse_input():
    f = open("input22.txt", 'r')
    contents = f.read().split('\n')
    f.close()
    commands = []
    for content in contents:
        if "deal with increment" in content:
            result = [int(d) for d in re.findall(r'-?\d+', content)]
            commands.append((DEAL_WITH_INCREMENT, result[0]))
        elif "deal into new stack" in content:
            commands.append((DEAL_INTO_NEW_STACK, 0))
        elif "cut" in content:
            result = [int(d) for d in re.findall(r'-?\d+', content)]
            commands.append((CUT, result[0]))
    return commands

class MinimalDeck:
    def __init__(self, length):
        self.length = length
        self.num_shuffles = 0
        self.prev_start_card = 0
        self.cur_start_card = 2544

    def do_shuffle(self):
        self.num_shuffles += 1
        self.prev_start_card = self.cur_start_card
        increment = 2544 * (27**(self.num_shuffles))
        new_val = self.cur_start_card + increment
        self.cur_start_card = new_val % self.length

    def get_card_in_pos(self, pos):
        increment = pos * (27**(self.num_shuffles))
        new_val = self.prev_start_card + increment
        return new_val % self.length

    def get_pos_of_card(self, card):
        for i in range(self.length):
            if card == self.get_card_in_pos(i):
                return i
        return None

class Deck:
    def __init__(self, len):
        self.cards = list(range(0, len))
        self.len = len
        self.num_shuffles = 0
        self.prev_start_card = 0

    def print_deck(self):
        print(self.cards)

    def deal_into_new_stack(self):
        self.cards.reverse()

    def cut_n_cards(self, n):
        self.cards = self.cards[n:] + self.cards[0:n]

    def deal_with_increment_n(self, n):
        space_table = [None] * self.len
        space_table_index = 0
        for card in self.cards:
            space_table[space_table_index] = card
            space_table_index = (space_table_index + n) % self.len
        self.cards = space_table
    
    def get_pos_of_card(self, card):
        for index, elem in enumerate(self.cards):
            if elem == card:
                return index

    def do_shuffle(self, commands):
        for command, n in commands:
            if command == DEAL_INTO_NEW_STACK:
                deck.deal_into_new_stack()
            elif command == DEAL_WITH_INCREMENT:
                deck.deal_with_increment_n(n)
            elif command == CUT:
                deck.cut_n_cards(n)
            else:
                print("faulty command")
        self.num_shuffles += 1

    def do_shuffle_faster(self):
        starting_card = self.cards[2544]
        for i in range(0, self.len):
            #print("did a thing")
            increment = i * (27**(self.num_shuffles + 1))
            new_val = starting_card + increment
            self.cards[i] = new_val % self.len
        self.num_shuffles += 1

    def compare_to(self, minimal_deck):
        for i in range(0, len(self.cards)):
            if self.cards[i] != minimal_deck.get_card_in_pos(i):
                print(f"not the same.. index {i} card1 {self.cards[i]} card2 {minimal_deck.get_card_in_pos(i)}")
        print("=============DONE WITH COMPARE================================")

        
def calc_num(a, k, n):
    # a*((k**n) - 1) / (k - 1)
    return (a*(pow(k, n) - 1) / (k - 1)) % 119315717514047
    #return a*pow(k, n, 119315717514047) / (k - 1)
    #((a*((k**n) - 1) / (k - 1)) % 119315717514047) - 
    #return a*((k**n) - 1) / (k - 1) % 119315717514047

if __name__ == '__main__':
    #commands = parse_input()
    #min_deck = MinimalDeck(10007)
    #deck = Deck(10007)
    #index = 100
    #for i in range(0, index):
    #    deck.do_shuffle(commands)
    #    min_deck.do_shuffle()
    #    deck.compare_to(min_deck)
    pos = 2020
    len_of_deck = 119315717514047
    min_deck_part_b = MinimalDeck(len_of_deck)
    for i in range(20):
        card = min_deck_part_b.get_card_in_pos(pos)
        print(f"n: {i} card: {card}")
        min_deck_part_b.do_shuffle()
    # 101741582076661
    a = 2544
    k = 27
    n = 101741582076661
    #n = 17
    start_card = calc_num(a, k, n) # -1?

    # Calc the 2020 card
    increment = 2020 * pow(k, n)
    new_val = (start_card + increment) % len_of_deck
    print(f"final calc with n: {n} and card is {new_val}")



