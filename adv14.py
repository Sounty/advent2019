import unittest
import re
import math
import copy
from collections import deque

class recipe:
    def __init__(self, name, quantity):
        self.ingridients = []
        self.name = name
        self.quantity = quantity

# Create a map with key: (1, "FUEL") -> [(2, "A"), (3, "B")] etc
def parse_input(input):
    recepies = {}
    lst = input.split('\n')
    for row in lst:
        lel = row.split("=>")
        temp = []
        ingridients = lel[0].strip()
        split_ingridients = ingridients.split(',')
        for ing in split_ingridients:
            groups = re.search(r"(\d+) ([A-z]+)", ing).groups()
            temp.append((int(groups[0]), groups[1]))
        result = lel[1].strip()
        groups = re.search(r"(\d+) ([A-z]+)", result).groups()
        recepies[(int(groups[0]), groups[1])] = temp
    return recepies

def find_recipe(recepies, ingridient):
    found_key = None
    for key in recepies.keys():
        if key[1] == ingridient:
            if found_key != None:
                print(key)
            found_key = key
    return found_key

def print_resources(resources):
    print("==============")
    for key, val in resources.items():
        print(f"Have {val} of {key}")

def add_resource(resources, resource, amount):
    if resources.get(resource, 0) == 0:
        resources[resource] = amount
    else:
        resources[resource] += amount

def consume_existing_resources(ingridient_list, basic_resources_left):
    for index in range(len(ingridient_list)):
        needed, ingridient = ingridient_list[index]
        while basic_resources_left.get(ingridient, 0) > 0 and needed > 0:
            print(f"Used 1 of ingridient {ingridient} without buying new")
            basic_resources_left[ingridient] -= 1
            needed -= 1
        ingridient_list[index] = (needed, ingridient)

def buy_ingridients(recepies, result, multiplier, basic_resources_left, sum):
    ingridient_list = recepies.get(result)
    for needed_for_1_stack, ingridient in ingridient_list:
        total_needed_amount = multiplier * needed_for_1_stack
        if ingridient == "ORE":
            #purchase some basic resource
            sum.total = sum.total + total_needed_amount
        else:
            # Are there resources we can use?
            while basic_resources_left.get(ingridient, 0) > 0 and total_needed_amount > 0:
                basic_resources_left[ingridient] -= 1
                total_needed_amount -= 1                
            if total_needed_amount > 0:
                key = find_recipe(recepies, ingridient)
                new_multiplier = math.ceil(total_needed_amount / key[0])
                buy_ingridients(recepies, key, new_multiplier, basic_resources_left, sum)
                num_bought = key[0]*new_multiplier
                add_resource(basic_resources_left, ingridient, num_bought - total_needed_amount)

class SumHolder:
    def __init__(self):
        self.total = 0

def calc_needed_ores(recepies, start_key):
    basic_resources_left = {}
    sum = SumHolder()
    buy_ingridients(recepies, start_key, start_key[0] , basic_resources_left, sum)
    #print_resources(basic_resources_left)
    return sum.total

def try_fuel(recepies, val, cur_key):
    tmp = recepies[cur_key]
    del recepies[cur_key]
    new_key = (val, "FUEL")
    recepies[new_key] = tmp
    return (new_key, calc_needed_ores(recepies, new_key))

def find_max_for_one_trillion_ore(input):
    recepies = parse_input(input)
    prev_key = (1, "FUEL")
    val_to_try = 1000000000000 # Go big or go home eh?
    while True:
        # prev value
        new_key, ores = try_fuel(recepies, val_to_try, prev_key)
        if ores > 1000000000000:
            if abs(val_to_try - prev_key[0]) == 1:
                decrease_amount = 1
            else:
                decrease_amount = math.ceil(abs(val_to_try - prev_key[0]) / 2)
            val_to_try -= decrease_amount
        elif ores < 1000000000000:
            increase_amount = math.ceil(abs(prev_key[0] - val_to_try) / 2)
            if increase_amount == 1:
                # Always return on the smaller side
                return val_to_try
            val_to_try += increase_amount
        else:
            # Not likley but maybe? =)
            break
        prev_key = new_key
    return val_to_try


def get_min_ore_to_produce_1_fuel(input):
    recepies = parse_input(input)
    total_ores = calc_needed_ores(recepies, (1, "FUEL"))
    return total_ores


class TestFuelConsumer(unittest.TestCase):
    def test_small_program(self):
        input = '''10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL'''
        self.assertEqual(get_min_ore_to_produce_1_fuel(input), 31)

        input = '''9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL'''
        self.assertEqual(get_min_ore_to_produce_1_fuel(input), 165)

    def test_larger_examples(self):
        input = '''157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT'''
        self.assertEqual(get_min_ore_to_produce_1_fuel(input), 13312)
        self.assertEqual(find_max_for_one_trillion_ore(input), 82892753)

        input = '''2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF'''
        self.assertEqual(get_min_ore_to_produce_1_fuel(input), 180697)
        self.assertEqual(find_max_for_one_trillion_ore(input), 5586022)

        input = '''171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX'''
        self.assertEqual(get_min_ore_to_produce_1_fuel(input), 2210736)
        self.assertEqual(find_max_for_one_trillion_ore(input), 460664)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFuelConsumer)
    unittest.TextTestRunner(verbosity=2).run(suite)

    # Part a
    print("PART A")
    f = open("input14.txt", 'r')
    input = f.read()
    f.close()
    print(get_min_ore_to_produce_1_fuel(input))

    # part b
    print("PART B")
    print(find_max_for_one_trillion_ore(input))