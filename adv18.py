from collections import deque
from collections import defaultdict
import math
import string
import copy

def find_where_we_at(the_map):
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            if the_map[y][x] == '@':
                return (x, y)

def print_map(the_map):
    for y in range(len(the_map)):
        row = []
        for x in range(len(the_map[y])):
            row.append(the_map[y][x])
        print("".join(row), sep="")
    
def validator_wall(char):
    return char != '#'

def validator_can_move(char):
    return char == '.' or char.islower() or char == '@'

def get_all_valid_neighbors(the_map, cur_pos, validator):
    x = cur_pos[0]
    y = cur_pos[1]
    valid_paths = []
    if x > 0:
        char = the_map[y][x-1]
        if validator(char):
            valid_paths.append((x - 1, y))
    if x < len(the_map[0]) - 1:
        char = the_map[y][x+1]
        if validator(char):
            valid_paths.append((x+1, y))
    if y > 0:
        char = the_map[y-1][x]
        if validator(char):
            valid_paths.append((x, y-1))
    if y < len(the_map) - 1:
        char = the_map[y+1][x]
        if validator(char):
            valid_paths.append((x, y+1))
    
    return valid_paths

def unlock_key(key, the_map):
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            if the_map[y][x] == key.upper():
                the_map[y][x] = '.'

def find_keys(the_map):
    keys = []
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            char = the_map[y][x]
            if char.islower():
                keys.append(char)
    return keys

def get_letter_coord(the_map, letter):
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            char = the_map[y][x]
            if char == letter:
                return (x, y)
    return None

def get_available_keys(from_letter, the_map):
    keys = []
    start_coord = get_letter_coord(the_map, from_letter)
    the_queue = deque()
    discovered = set()
    discovered.add(start_coord)
    the_queue.append(start_coord)
    # Flood fill
    while the_queue:
        cur_pos = the_queue.pop()
        to_letter = the_map[cur_pos[1]][cur_pos[0]]
        if to_letter.islower():
            keys.append(to_letter)
        for n in get_all_valid_neighbors(the_map, cur_pos, validator_can_move):
            if not n in discovered:
                the_queue.append(n)
                discovered.add(n)
    return keys

def get_cost(from_letter, to_letter, the_map, cost_table):
    cost = cost_table.get((from_letter, to_letter), math.inf)
    if cost != math.inf:
        return cost
        #print(f"cost from {from_letter} to {to_letter} is {cost}")
        #return cost

    start_coord = get_letter_coord(the_map, from_letter)
    the_queue = deque()
    discovered = set()
    discovered.add(start_coord)
    the_queue.append(start_coord)
    new_steps = 0
    # Flood fill
    while the_queue:
        lst = []
        while the_queue:
            lst.append(the_queue.pop())
        for cur_pos in lst:
            key = the_map[cur_pos[1]][cur_pos[0]]
            if key.islower():
                tup = (from_letter, key)
                cost_table[tup] = new_steps
                #print(f"cost from {from_letter} calculated to {key}")
                #if cost_table.get(tup, math.inf) == math.inf:
            for n in get_all_valid_neighbors(the_map, cur_pos, validator_can_move):
                if not n in discovered:
                    the_queue.append(n)
                    discovered.add(n)
        new_steps += 1
    return cost_table.get((from_letter, to_letter), math.inf)

def get_min_cost_path_left(remaining_keys, from_letter, min_cost_library):
    as_string = "".join(remaining_keys)
    min_cost = min_cost_library.get((as_string, from_letter), None)
    #if min_cost != None:
        #print(f"looked up {(as_string, from_letter)} to be val {min_cost}")
    return min_cost

def fill_in_min_cost_lib(remaining_keys, from_letter, min_cost_library, val):
    as_string = "".join(remaining_keys)
    #print(f"added {(as_string, from_letter)} as val {val}")
    min_cost_library[(as_string, from_letter)] = val


def find_shortest_r(cost_table, remaining_keys, cur_min_cost, from_letter, the_map, path_library, consumed_keys, min_cost_library):
    if len(remaining_keys) == 0:
        return 0

    min_cost = get_min_cost_path_left(remaining_keys, from_letter, min_cost_library)
    if min_cost != None:
        return min_cost

    min_cost = math.inf
    avail_paths = get_avail_paths(consumed_keys, remaining_keys, the_map, path_library)
    for to_letter in avail_paths:
        added_cost = get_cost(from_letter, to_letter, the_map, cost_table)
        #if cost + added_cost >= cur_min_cost[0]:
        #    continue
        new_keys = copy.deepcopy(remaining_keys)
        new_keys.remove(to_letter)
        new_keys = sorted(new_keys)
        new_consumed_key = copy.deepcopy(consumed_keys)
        new_consumed_key.append(to_letter)
        new_consumed_key = sorted(new_consumed_key)
        new_map = copy.deepcopy(the_map)
        unlock_key(to_letter, new_map)

        val = added_cost + find_shortest_r(cost_table, new_keys, cur_min_cost, to_letter, new_map, path_library, new_consumed_key, min_cost_library)
        min_cost = min(min_cost, val)
        fill_in_min_cost_lib(new_keys, to_letter, min_cost_library, val - added_cost)

    return min_cost

def get_avail_paths(consumed_keys, keys_left, the_map, path_library):
    if len(consumed_keys) == 0:
        consumed_keys.append('@')
    as_string = "".join(consumed_keys)
    avail_paths = path_library.get(as_string, None)
    if avail_paths == None:
        # Build that path and see what keys are available
        path_library[as_string] = get_available_keys(consumed_keys[0], the_map)

    non_consumed_avail_keys = []
    for key in path_library.get(as_string):
        if not key in consumed_keys:
            non_consumed_avail_keys.append(key)

    return non_consumed_avail_keys

def find_starting_positions(the_map):
    poses = []
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            if the_map[y][x] == '@':
                poses.append((x, y))
    return poses

class Maze4Compartments:
    def __init__(self, the_map, remaining_keys):
        self.the_map = the_map
        self.cur_positions = find_starting_positions(the_map)
        self.total_remaining_keys = remaining_keys

    def take_all_the_keys(self, maze_quadrant):
        cur_pos = self.cur_positions[maze_quadrant]
        the_queue = deque()
        discovered = set()
        discovered.add(cur_pos)
        the_queue.append(cur_pos)
        steps = 0
        # Flood fill
        while the_queue:
            lst = []
            while the_queue:
                lst.append(the_queue.pop())
            for cur_pos in lst:
                # did we consume a number?
                
                for n in get_all_valid_neighbors(the_map, cur_pos, validator_can_move):
                    if not n in discovered:
                        the_queue.append(n)
                        discovered.add(n)
            steps += 1

if __name__ == '__main__':
    f = open("input18.txt", 'r')
    content = f.read()
    f.close()
    lines = content.split('\n')
    the_map = [list(x) for x in lines]

    remaining_keys = find_keys(the_map)
    #consumed_keys = []
    #cost_table = {}
    #cur_min_cost = [math.inf]
    ##cur_min_cost = [6788]
    #path_library = {}
    #min_cost_library = {}
#
    ##update_cost_table(the_map, cost_table, '@', all_keys)
    #minimal = find_shortest_r(cost_table, remaining_keys, cur_min_cost, '@', the_map, path_library, consumed_keys, min_cost_library)
    #print("finally", minimal)
    #print(path_library['@'])

    start_positions = find_starting_positions(the_map)
    print(start_positions)


    
