import sys
import unittest
import re
import numpy as np
import copy

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def isEqual(self, point):
        return (self.x == point.x) and (self.y == point.y)
    def getDistance(self, point):
        return abs(self.x - point.x) + abs(self.y - point.y)

class Line:
    def __init__(self, point1, point2):
        self.point1 = point1
        self.point2 = point2
    def isPointOnLine(self, point):
        y_min = min(self.point1.y, self.point2.y)
        y_max = max(self.point1.y, self.point2.y)
        x_min = min(self.point1.x, self.point2.x)
        x_max = max(self.point1.x, self.point2.x)
        if x_max < point.x or x_min > point.x:
            return False
        if y_max < point.y or y_min > point.y:
            return False
        for x in range(x_min, x_max + 1):
            if x != point.x:
                continue
            for y in range(y_min, y_max + 1):
                if x == point.x and y == point.y:
                    return True
        return False
    def getLength(self):
        return self.point1.getDistance(self.point2)

def get_lines(path):
    startPoint = Point(0, 0)
    lines = []
    for val in path:
        amount = int(re.search(r'\d+', val).group())
        direction = val[0]
        endPoint = Point(startPoint.x, startPoint.y)
        if direction == 'R':
            endPoint.x += amount
        if direction == 'D':
            endPoint.y -= amount
        if direction == 'U':
            endPoint.y += amount
        if direction == 'L':
            endPoint.x -= amount
        line = Line(startPoint, endPoint)
        startPoint = copy.deepcopy(endPoint)
        lines.append(line)
    return lines

def find_intersection(line1, line2):
    y1_min = min(line1.point1.y, line1.point2.y)
    y1_max = max(line1.point1.y, line1.point2.y)
    x1_min = min(line1.point1.x, line1.point2.x)
    x1_max = max(line1.point1.x, line1.point2.x)

    y2_min = min(line2.point1.y, line2.point2.y)
    y2_max = max(line2.point1.y, line2.point2.y)
    x2_min = min(line2.point1.x, line2.point2.x)
    x2_max = max(line2.point1.x, line2.point2.x)
    
    # Check if no chance
    if x1_max < x2_min or x2_max < x1_min:
        return Point(0,0)
    if y1_max < y2_min or y2_max < y1_min:
        return Point(0,0)
    
    for y1 in range(y1_min, y1_max + 1):
        for y2 in range(y2_min, y2_max + 1):
            if y1 != y2:
                continue
            for x1 in range(x1_min, x1_max + 1):
                for x2 in range(x2_min, x2_max + 1):
                    if x1 == x2 and y1 == y2:
                        return Point(x1, y1)
    return Point(0, 0)

def find_intersections(lines_first_wire, lines_second_wire):
    intersections = []
    for line1 in lines_first_wire:
        for line2 in lines_second_wire:
            intersection = find_intersection(line1, line2)
            if not intersection.isEqual(Point(0,0)):
                intersections.append(intersection)
    return intersections

def get_shortest_distance(firstPath, secondPath):
    lines_first_wire = get_lines(firstPath)
    lines_second_wire = get_lines(secondPath)
    
    intersections = find_intersections(lines_first_wire, lines_second_wire)

    #finally calc all distances
    shortest_dist = -1
    for intersection in intersections:
        dist = abs(intersection.x) + abs(intersection.y)
        if shortest_dist > dist or shortest_dist == -1:
            shortest_dist = dist
    
    return shortest_dist

def get_distance_via_wire(lines, point):
    distance = 0
    for line in lines:
        if line.isPointOnLine(point):
            distance += line.point1.getDistance(point)
            break
        else:
            distance += line.getLength()
    return distance

def get_min_distance_via_wire(firstPath, secondPath):
    # First: find all intersections
    lines_first_wire = get_lines(firstPath)
    lines_second_wire = get_lines(secondPath)
    intersections = find_intersections(lines_first_wire, lines_second_wire)

    min_total_distance = -1
    for intersection in intersections:
        dist_sum = get_distance_via_wire(lines_first_wire, intersection) + get_distance_via_wire(lines_second_wire, intersection)
        if min_total_distance == -1:
            min_total_distance = dist_sum
        elif min_total_distance > dist_sum:
            min_total_distance = dist_sum
    return min_total_distance
    
def main_a():
    pathToFile = "input3.txt"
    f = open(pathToFile, 'r')
    lines = f.readlines()
    path1 = lines[0].split(',')
    path2 = lines[1].split(',')
    f.close()
    return get_shortest_distance(path1, path2)
def main_b():
    pathToFile = "input3.txt"
    f = open(pathToFile, 'r')
    lines = f.readlines()
    path1 = lines[0].split(',')
    path2 = lines[1].split(',')
    f.close()
    return get_min_distance_via_wire(path1, path2)

class TestIntersectionFinder(unittest.TestCase):
    def test_calc_closest_distance(self):
        self.assertEqual(get_shortest_distance(["R8","U5","L5","D3"], ["U7","R6","D4","L4"]), 6)
        self.assertEqual(get_shortest_distance(["R75","D30","R83","U83","L12","D49","R71","U7","L72"], ["U62","R66","U55","R34","D71","R55","D58","R83"]), 159)
        self.assertEqual(get_shortest_distance(["R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"], ["U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"]), 135)
    
    def test_calc_closest_distance_via_wire(self):
        self.assertEqual(get_min_distance_via_wire(["R75","D30","R83","U83","L12","D49","R71","U7","L72"], ["U62","R66","U55","R34","D71","R55","D58","R83"]), 610)
        self.assertEqual(get_min_distance_via_wire(["R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"], ["U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"]), 410)

    def test_part_a(self):
        self.assertEqual(main_a(), 303)

    def test_part_b(self):
        self.assertEqual(main_b(), 11222)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestIntersectionFinder)
    unittest.TextTestRunner(verbosity=2).run(suite)