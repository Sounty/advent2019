import unittest
import math

def parse_input(input):
    content = input.split('\n')
    map = []
    for row in content:
        map_row = []
        for c in row:
            map_row.append(c)
        map.append(map_row)
    return map

def add_angle_if_not_exist(point1, point2, angles):
    angle = math.atan2(point2[1]-point1[1], point2[0]-point1[0])
    if len(angles) == 0:
        angles.add(angle)
    else:
        if not angle in angles:
            angles.add(angle)

def build_angle_library(input):
    angle_lib = {} # (x, y) as key, set of angles as value
    for y in range(len(input)):
        for x in range(len(input[0])):
            if input[y][x] == '#':
                angle_lib[(x, y)] = set()
    # Now fill it with angles
    for point1, angles1 in angle_lib.items():
        for point2, angles2 in angle_lib.items():
            if point1 == point2:
                continue
            add_angle_if_not_exist(point1, point2, angles1)
    return angle_lib

def get_max_seen(angle_lib):
    max_seen = 0
    max_point = (-1, -1)
    for point, angles in angle_lib.items():
        num_seen = len(angles)
        if num_seen > max_seen:
            max_seen = num_seen
            max_point = point
    return (max_seen, max_point)

def get_200th_shotdown_ast(angle_lib, point):
    # Sort angles for the point
    angles = sorted(angle_lib[point])
    startIndex = 0
    index_of_200_asteroid = 0
    while startIndex < len(angles):
        if angles[startIndex] == (math.pi/-2): # We are pointing up
            index_of_200_asteroid = (startIndex + 199) % len(angles)
            break
        startIndex += 1
    interesting_asts = get_asteroid_coords(angles[index_of_200_asteroid], point, angle_lib)
    # We need to find the closest if more than one. But it is only one so fuck it
    if len(interesting_asts) == 1:
        return interesting_asts[0]
    return (0,0) # care

def get_asteroid_coords(angle, point, angle_lib):
    points_of_interest = []
    for point2, angles in angle_lib.items():
        cur_angle = math.atan2(point2[1]-point[1], point2[0]-point[0])
        if angle != cur_angle:
            continue
        points_of_interest.append(point2)
    return points_of_interest

class TestAstLel(unittest.TestCase):
    def test_small_program(self):
        input = '''.#..#
.....
#####
....#
...##'''
        angle_lib = build_angle_library(parse_input(input))
        seen, point = get_max_seen(angle_lib)
        self.assertEqual(seen, 8)
        self.assertEqual(point, (3, 4.0))

    def test_slightly_bigger(self):
        input = '''......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####'''
        angle_lib = build_angle_library(parse_input(input))
        seen, point = get_max_seen(angle_lib)
        self.assertEqual(seen, 33)
        self.assertEqual(point, (5, 8.0))

        input = '''#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.'''
        angle_lib = build_angle_library(parse_input(input))
        seen, point = get_max_seen(angle_lib)
        self.assertEqual(seen, 35)
        self.assertEqual(point, (1, 2.0))

        input = '''.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..'''
        angle_lib = build_angle_library(parse_input(input))
        seen, point = get_max_seen(angle_lib)
        self.assertEqual(seen, 41)
        self.assertEqual(point, (6, 3.0))

        input = '''..#.
.###
..#.''' 
        angle_lib = build_angle_library(parse_input(input))
        seen, point = get_max_seen(angle_lib)
        self.assertEqual(seen, 4)
        self.assertEqual(point, (2, 1))

        input = '''.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##'''
        angle_lib = build_angle_library(parse_input(input))
        seen, point = get_max_seen(angle_lib)
        self.assertEqual(seen, 210)
        self.assertEqual(point, (11, 13.0))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAstLel)
    unittest.TextTestRunner(verbosity=2).run(suite)
    # Part a
    f = open("input10.txt", 'r')
    input = f.read()
    angle_lib = build_angle_library(parse_input(input))

    # part a
    seen, point = get_max_seen(angle_lib)
    print("======PART A======")
    print(f"Seen: {seen}, point: {point}")

    # part b
    print("======PART B======")
    print(get_200th_shotdown_ast(angle_lib, point))
    f.close()