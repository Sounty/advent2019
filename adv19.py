from adv9 import IntCodeInterpreter
import math


def getMemory():
    f = open("input19.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

class DroneSystem:
    def __init__(self):
        self.map = {}
        self.init_mem = getMemory()

    def deploy_drone(self, x, y):
        # Refresh memory
        brain = IntCodeInterpreter(self.init_mem, [x, y])
        res = next(brain.runIntCode())
        self.map[(x, y)] = res
        return res

    def print_map(self):
        max_x = 0
        max_y = 0
        for key in self.map.keys():
            if key[0] > max_x:
                max_x = key[0]
            if key[1] > max_y:
                max_y = key[1]
        for y in range(0, max_y + 1):
            row = []
            for x in range(0, max_x + 1):
                val = self.map.get((x, y), 0)
                if val == 0:
                    row.append('.')
                else:
                    row.append('#')
            print("".join(row), sep="")

    def is_x_row_valid(self, x, y, box_size):
        if self.map.get((x, y - box_size + 1), 0) == 1:
            return True
        return False 

def get_min_square(drone_system, box_size):
    offset = 10
    start_x = 1
    for y in range(box_size + offset, 100000):
        lel = find_x_correct(drone_system, y, box_size, start_x)
        if lel[0]:
            return (lel[1], y - box_size)
        else:
            start_x = lel[1]

def find_x_correct(drone_system, y, box_size, lower):
    cnt = 0
    started_counting = False
    start_x = 0
    for x in range(lower, lower + 10000000):
        res = drone_system.deploy_drone(x, y)
        if res == 1:
            if not started_counting:
                start_x = x
            started_counting = True
            cnt += 1
            if cnt == box_size:
                # start checking
                if drone_system.is_x_row_valid(x, y, box_size):
                    return (True, x)
        else:
            if started_counting:
                #print("counted ", cnt)
                return (False, start_x)


if __name__ == '__main__':
    drone_system = DroneSystem()

    box_size = 100
    coord = get_min_square(drone_system, box_size)
    print(f"x is {coord[0] - box_size + 1} y is {coord[1] + 1}")
    print(f"calc val is {(coord[0] - box_size + 1)*10000 + coord[1] + 1}")
    
