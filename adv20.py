from collections import deque
import math

def get_all_interesting_neighbors(the_map, coord):
    x = coord[0]
    y = coord[1]
    n = []
    if x < len(the_map[0]) - 1:
        new_x = x + 1
        if the_map[y][new_x].isupper() or the_map[y][new_x] == '.':
            n.append((new_x, y))
    if y < len(the_map) - 1:
        new_y = y + 1
        if the_map[new_y][x].isupper() or the_map[new_y][x] == '.':
            n.append((x, new_y))
    if x > 0:
        new_x = x - 1
        if the_map[y][new_x].isupper() or the_map[y][new_x] == '.':
            n.append((new_x, y))
    if y > 0:
        new_y = y - 1
        if the_map[new_y][x].isupper() or the_map[new_y][x] == '.':
            n.append((x, new_y))
    return n

def get_next_dot_coord(the_map, coord):
    neighbors = get_all_interesting_neighbors(the_map, coord)
    for neighbor in neighbors:
        if the_map[neighbor[1]][neighbor[0]] == '.':
            return neighbor
    return None

def get_next_letter(the_map, coord):
    x = coord[0]
    y = coord[1]
    if x < len(the_map[0]) - 1:
        new_x = x + 1
        if the_map[y][new_x].isupper():
            coord =(new_x, y) 
            return (the_map[y][new_x], coord)
    if y < len(the_map) - 1:
        new_y = y + 1
        if the_map[new_y][x].isupper():
            coord =(x, new_y) 
            return (the_map[new_y][x], coord)
    return None

def get_next_letter_all_directions(the_map, coord):
    ns = get_all_interesting_neighbors(the_map, coord)
    for n in ns:
        if the_map[n[1]][n[0]].isupper():
            return (the_map[n[1]][n[0]], n)
    return None

def build_portal_map(the_map):
    # "AB" -> [coord1, coord2]
    portal_positions = {}
    for y in range(len(the_map)):
        for x in range(len(the_map[0])):
            if the_map[y][x].isupper():
                first_uppercase = the_map[y][x]
                next_stuff = get_next_letter(the_map, (x, y))
                if next_stuff == None:
                    continue
                coord = get_next_dot_coord(the_map, (x, y))
                if coord == None:
                    coord = get_next_dot_coord(the_map, next_stuff[1])
                if coord != None:
                    # B'A'.
                    my_str = "".join( [first_uppercase, next_stuff[0]])
                    if my_str == 'AA' or my_str == 'ZZ':
                        continue
                    lst = portal_positions.get(my_str, None)
                    if lst == None:
                        lst = []
                        portal_positions[my_str] = lst
                    portal_positions[my_str].append(coord)
    return portal_positions

def find_start(the_map):
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            if the_map[y][x] == 'A':
                next_stuff = get_next_letter(the_map, (x, y))
                if next_stuff == None:
                    continue
                if next_stuff[0] == 'A':
                    coord = get_next_dot_coord(the_map, (x, y))
                    if coord == None:
                        coord = get_next_dot_coord(the_map, next_stuff[1])
                    if coord != None:
                        return coord                        
        
def check_if_done(the_map, cur_pos, layer):
    if layer != 0:
        return False
    char = the_map[cur_pos[1]][cur_pos[0]]
    if char.isupper() and char == 'Z':
        adjecent, coord = get_next_letter_all_directions(the_map, cur_pos)
        if adjecent == 'Z':
            return True
    return False

def walk_through_portal(cur_pos, portal_map):
    char = the_map[cur_pos[1]][cur_pos[0]]
    if not char.isupper():
        return None
    next_letter, coord = get_next_letter_all_directions(the_map, cur_pos)
    if char == 'A' and next_letter == 'A':
        return None
    dot_coord = get_next_dot_coord(the_map, cur_pos)
    for key, connected_coords in portal_map.items():
        if dot_coord in connected_coords:
            if dot_coord == connected_coords[0]: 
                return connected_coords[1]
            return connected_coords[0]
    return None

def flood_fill(the_map, start_coord, portal_map):
    #start_coord = get_letter_coord(the_map, from_letter)
    the_queue = deque()
    discovered = set()
    discovered.add(start_coord)
    the_queue.append(start_coord)
    steps = 0
    # Flood fill
    while the_queue:
        lst = []
        while the_queue:
            lst.append(the_queue.pop())
        for cur_pos in lst:
            # Are we done?
            if check_if_done(the_map, cur_pos, 0):
                return steps
            # Did we walk through a portal?
            maybe_new_pos = walk_through_portal(cur_pos, portal_map)
            if maybe_new_pos != None:
                cur_pos = maybe_new_pos
            
            for n in get_all_interesting_neighbors(the_map, cur_pos):
                if not n in discovered:
                    the_queue.append(n)
                    discovered.add(n)
        steps += 1

def is_portal_outer(the_map, coord, portal_map):
    # it brings you up one level
    x = coord[0]
    y = coord[1]
    if x <= 4: # LEFT
        return True
    if y <= 4: # UPPER
        return True 
    if x >= len(the_map[0]) - 5: # RIGHT
        return True
    if y >= len(the_map) - 5: # BOTTOM
        return True

def is_portal_inner(the_map, coord, portal_map):
    # it brings you down one level
    return not is_portal_outer(the_map, coord, portal_map)

def walk_through_portal_part_b(the_map, cur_pos, portal_map, layer):
    char = the_map[cur_pos[1]][cur_pos[0]]
    if not char.isupper():
        return None
    next_letter, coord = get_next_letter_all_directions(the_map, cur_pos)
    if char == 'A' and next_letter == 'A':
        return None
    dot_coord = get_next_dot_coord(the_map, cur_pos)
    if dot_coord is None:
        return None
    if layer == 0 and is_portal_outer(the_map, dot_coord, portal_map):
        # can only move down
        return None
    for key, connected_coords in portal_map.items():
        if dot_coord in connected_coords:
            if dot_coord == connected_coords[0]: 
                return connected_coords[1]
            return connected_coords[0]
    return None

def flood_fill_part_b(the_map, start_coord, portal_map):
    the_queue = deque()
    discovered = set()
    discovered.add(start_coord)
    the_queue.append(start_coord)
    steps = 0
    # Flood fill
    while the_queue:
        lst = []
        while the_queue:
            lst.append(the_queue.pop())
        for x, y, layer in lst:
            cur_pos = (x, y)
            # Are we done?
            if check_if_done(the_map, cur_pos, layer):
                return steps
            # Did we walk through a portal?
            maybe_new_pos = walk_through_portal_part_b(the_map, cur_pos, portal_map, layer)
            if maybe_new_pos != None:
                # update layer
                if is_portal_inner(the_map, cur_pos, portal_map):
                    layer += 1
                elif is_portal_outer(the_map, cur_pos, portal_map):
                    layer -= 1
                cur_pos = maybe_new_pos
            for n in get_all_interesting_neighbors(the_map, cur_pos):
                n_w_layer = (n[0], n[1], layer)
                if not n_w_layer in discovered:
                    the_queue.append(n_w_layer)
                    discovered.add(n_w_layer)
        steps += 1

if __name__ == '__main__':
    f = open("input20.txt", 'r')
    content = f.read()
    f.close()
    lines = content.split('\n')
    the_map = [list(x) for x in lines]

    start = find_start(the_map)
    portal_map = build_portal_map(the_map)
    #for key, val in portal_map.items():
    #    print(f"{key} conntecting {val}")

    print("PART A")
    res = flood_fill(the_map, start, portal_map)
    print(res - 1)

    print("PART B")
    res = flood_fill_part_b(the_map, (start[0], start[1], 0), portal_map)
    print(res - 1)