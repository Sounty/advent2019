import sys
import unittest

def calcFuelCost(mass):
  return int(mass / 3) - 2

def calcFuelCostFuelInclusive(mass):
    fuelCostThisModule = 0
    while True:
        fuelCost = calcFuelCost(mass)
        if fuelCost <= 0:
            break
        mass = fuelCost
        fuelCostThisModule += fuelCost
    return fuelCostThisModule

def main(calcFunc):
    pathToFile = "input1.txt"
    f = open(pathToFile, 'r')
    lines = f.readlines()
    f.close()
    fuelSum = 0
    for line in lines:
        mass = int(line)
        fuelSum += calcFunc(mass)
    return fuelSum

class TestFuelCost(unittest.TestCase):
    def test_calc_fuel(self):
        self.assertEqual(calcFuelCost(12), 2)
        self.assertEqual(calcFuelCost(14), 2)
        self.assertEqual(calcFuelCost(1969), 654)
        self.assertEqual(calcFuelCost(100756), 33583)

    def test_part_a(self):
        self.assertEqual(main(calcFuelCost), 3422661)

    def test_calc_fuel_when_fuel_weighs(self):
        self.assertEqual(calcFuelCostFuelInclusive(14), 2)
        self.assertEqual(calcFuelCostFuelInclusive(1969), 966)
        self.assertEqual(calcFuelCostFuelInclusive(100756), 50346)

    def test_part_b(self):
        self.assertEqual(main(calcFuelCostFuelInclusive), 5131103)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFuelCost)
    unittest.TextTestRunner(verbosity=2).run(suite)