import unittest

def getMemory():
    f = open("input5.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

def parse_op_code_with_param_modes(number):
    param_modes = []
    op_code = -1
    while number:
        if op_code == -1:
            op_code = number % 100
            number = int(number / 100)
        divider = 10
        param_mode = number % divider
        param_modes.append(param_mode)
        number = int(number / divider)
    # Padd with extra 0's if needed
    if op_code == 1 or op_code == 2 or op_code == 7 or op_code == 8:
        while len(param_modes) != 3:
            param_modes.append(0)
    elif op_code == 3 or op_code == 4:
        while len(param_modes) != 1:
            param_modes.append(0)
    elif op_code == 5 or op_code == 6:
        while len(param_modes) != 2:
            param_modes.append(0)
    return (op_code, param_modes)

def add(value1, value2):
    return value1 + value2
def mult(value1, value2):
    return value1 * value2
def eq(value1, value2):
    if value1 == value2:
        return 1
    return 0
def lt(value1, value2):
    if value1 < value2:
        return 1
    return 0

def get_parameters(memory, param_modes, params, amount):
    values = []
    for i in range(0, amount):
        if param_modes[i] == 0:
            values.append(memory[params[i]])
        else:
            values.append(params[i])
    return values

def add_or_mult(memory, param_modes, params, func):
    values = get_parameters(memory, param_modes, params, 2)
    # Now write result to mem
    if param_modes[2] == 0:
        pos = params[2]
        memory[pos] = func(values[0], values[1])

def jump_if_true(memory, param_modes, params, pc):
    values = get_parameters(memory, param_modes, params, 2)
    if values[0] == 0:
        return pc + 3
    return values[1]

def jump_if_false(memory, param_modes, params, pc):
    values = get_parameters(memory, param_modes, params, 2)
    if values[0] != 0:
        return pc + 3
    return values[1]

def my_cmp(memory, param_modes, params, cmp_func):
    values = get_parameters(memory, param_modes, params, 2)
    if param_modes[2] == 0:
        pos = params[2]
        memory[pos] = cmp_func(values[0], values[1])

def increase_pointer(op_code, pc):
    if op_code == 1 or op_code == 2 or op_code == 7 or op_code == 8:
        return pc + 4
    elif op_code == 3 or op_code == 4:
        return pc + 2

def handle_output(memory, pc, param_modes, outputs):
    posOrValue = memory[pc + 1]
    if param_modes[0] == 0:
        outputs.append(memory[posOrValue])
    else:
        outputs.append(posOrValue)

def handle_input(memory, pc, inputs, input_pos):
    pos1 = memory[pc + 1]
    memory[pos1] = inputs[input_pos]
    return input_pos + 1

def runIntCode(memory, inputs):
    pc = 0
    input_pos = 0
    outputs = []
    while True:
        op_code, param_modes = parse_op_code_with_param_modes(memory[pc])
        if op_code == 99:
            break
        if op_code == 1:
            add_or_mult(memory, param_modes, memory[pc + 1: pc + 4], add)
        elif op_code == 2:
            add_or_mult(memory, param_modes, memory[pc + 1: pc + 4], mult)
        elif op_code == 3:
            input_pos = handle_input(memory, pc, inputs, input_pos)
        elif op_code == 4:
            handle_output(memory, pc, param_modes, outputs)
        elif op_code == 5:
            pc = jump_if_true(memory, param_modes, memory[pc + 1: pc + 3], pc)
            continue #This should not increase the pointer as the others shall
        elif op_code == 6:
            pc = jump_if_false(memory, param_modes, memory[pc + 1: pc + 3], pc)
            continue #This should not increase the pointer as the others shall
        elif op_code == 7:
            my_cmp(memory, param_modes, memory[pc + 1: pc + 4], lt)
        elif op_code == 8:
            my_cmp(memory, param_modes, memory[pc + 1: pc + 4], eq)
        pc = increase_pointer(op_code, pc)
    return outputs


class TestImprovedIntCode(unittest.TestCase):
    def test_small_program(self):
        inputs = [1]
        self.assertEqual(runIntCode([3,0,4,0,99], inputs), [1])
        inputs = [8]
        self.assertEqual(runIntCode([3,9,8,9,10,9,4,9,99,-1,8], inputs), [1])
        inputs = [7]
        self.assertEqual(runIntCode([3,9,8,9,10,9,4,9,99,-1,8], inputs), [0])
        inputs = [7]
        self.assertEqual(runIntCode([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], inputs), [999])
        inputs = [8]
        self.assertEqual(runIntCode([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], inputs), [1000])
        inputs = [9]
        self.assertEqual(runIntCode([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], inputs), [1001])

    def test_parse_op_code(self):
        self.assertEqual(parse_op_code_with_param_modes(1002), (2, [0, 1, 0]))
    
    def test_part_a(self):
        mem = getMemory()
        inputs = [1]
        outputs = runIntCode(mem, inputs)
        print(outputs)

    def test_part_b(self):
        mem = getMemory()
        inputs = [5]
        outputs = runIntCode(mem, inputs)
        print(outputs[0])

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestImprovedIntCode)
    unittest.TextTestRunner(verbosity=2).run(suite)
