from adv9 import IntCodeInterpreter
import copy

def getMemory():
    f = open("input17.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

NORTH = 0
EAST = 1
SOUTH = 2
WEST = 3

class Droid:
    def __init__(self, x, y, the_map):
        self.x = x
        self.y = y
        self.brain = IntCodeInterpreter(getMemory(), [])
        self.brain.memory[0] = 2
        self.direction = NORTH
        self.map = the_map

    # returns which definite direction the next path is
    def get_next_direction(self):
        next_dir = None
        if self.direction == NORTH or self.direction == SOUTH:
            if self.x > 0:
                if self.map[self.y][self.x - 1] == '#':
                    return WEST
            if self.x < len(self.map[0]) - 1:
                if self.map[self.y][self.x + 1] == '#':
                    return EAST
        if self.direction == WEST or self.direction == EAST:
            #if self.y < len(self.map) and self.y > 0:
            if self.y > 0:
                if self.map[self.y - 1][self.x] == '#':
                    return NORTH
            if self.y < len(self.map) - 1:
                if self.map[self.y + 1][self.x] == '#':
                    return SOUTH
            
        return None
        
    
    def get_turn(self, next_dir):
        assert(next_dir != self.direction)
        if self.direction == NORTH and next_dir == WEST:
            return 'L'
        if self.direction == NORTH and next_dir == EAST:
            return 'R'
        if self.direction == SOUTH and next_dir == WEST:
            return 'R'
        if self.direction == SOUTH and next_dir == EAST:
            return 'L'
        if self.direction == WEST and next_dir == NORTH:
            return 'R'
        if self.direction == WEST and next_dir == SOUTH:
            return 'L'
        if self.direction == EAST and next_dir == NORTH:
            return 'L'
        if self.direction == EAST and next_dir == SOUTH:
            return 'R'

    def get_num_of_avail_moves(self, next_dir):
        sum = 0
        if next_dir == NORTH:
            for i in reversed(range(0, self.y)):
                if self.map[i][self.x] == '#':
                    sum += 1
                else:
                    break
        if next_dir == SOUTH:
            for i in range(self.y + 1, len(self.map) - 1):
                if self.map[i][self.x] == '#':
                    sum += 1
                else:
                    break
        if next_dir == WEST:
            for i in reversed(range(0, self.x)):
                if self.map[self.y][i] == '#':
                    sum += 1
                else:
                    break
        if next_dir == EAST:
            for i in range(self.x + 1, len(self.map[0])):
                if self.map[self.y][i] == '#':
                    sum += 1
                else:
                    break
        return sum

    # Return example R, 8
    def get_next_set_of_moves(self):
        next_dir = self.get_next_direction()
        #print(f"trying to turn from {self.direction} to {next_dir}")
        if next_dir == None:
            # No more path to walk.
            return None
        next_turn = self.get_turn(next_dir)
        num = self.get_num_of_avail_moves(next_dir)
        return (next_turn, num)

    def do_move(self, move):
        turn, steps = move
        if turn == 'R':
            self.direction = ((self.direction + 1) % 4)
        elif turn == 'L':
            self.direction = ((self.direction - 1) % 4)
        if self.direction == NORTH:
            self.y -= steps
        if self.direction == SOUTH:
            self.y += steps
        if self.direction == WEST:
            self.x -= steps
        if self.direction == EAST:
            self.x += steps

    def input_main_seq(self, seq): # seq ['A', 'B'.. etc]
        for move in seq:
            self.brain.inputs.append(ord(move))
            self.brain.inputs.append(ord(','))
        self.brain.inputs.pop()
        self.brain.inputs.append(ord('\n'))

    def input_function(self, abc):
        for abc, num in abc:
            self.brain.inputs.append(ord(abc))
            self.brain.inputs.append(ord(','))
            # Does num need to split?
            for num_part in split_num(num):
                self.brain.inputs.append(ord(str(num_part)))
            self.brain.inputs.append(ord(','))
        self.brain.inputs.pop()
        self.brain.inputs.append(ord('\n'))
        

def find_intersection_coords(the_map):
    coords = []
    for y in range(len(the_map)):
        if y == 0 or y == len(the_map) - 2:
            continue
        for x in range(len(the_map[y])):
            if x == 0 or x == len(the_map[y]) - 2:
                continue
            if the_map[y][x] == '#':
                # are all neighbors '#'
                if the_map[y-1][x] == '#' and the_map[y+1][x] == '#' and the_map[y][x-1] == '#' and the_map[y][x+1] == '#':
                    coords.append((x, y))
    return coords

def find_robot_coord(the_map):
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            if the_map[y][x] == '^':
                return (x, y)

def split_num(num):
    part_num = []
    while num > 9:
        part_num.append(num % 10)
        num = int(num / 10)
    part_num.append(num)
    rev = []
    for i in reversed(range(len(part_num))):
        rev.append(part_num[i])
    return rev

def find_and_replace_token(lst, token, token_name): # token is a list of e.g. ('R', 12)
    index_lst = 0
    token_len = len(token)
    assert(token_len > 1)
    where_to_continue = 0
    while True:
        if index_lst > len(lst) - token_len:
            break
        found_token = True
        for i in range(token_len):
            if lst[index_lst + i] != token[i]:
                found_token = False
        if found_token:
            for i in range(token_len):
                lst.pop(index_lst)
            lst.insert(index_lst, token_name)
            if where_to_continue == 0:
                where_to_continue = index_lst + 1
        index_lst += 1
    return where_to_continue - 1

def count_tokens(lst, token, token_name):
    index_lst = 0
    token_len = len(token)
    assert(token_len > 1)
    token_count = 0
    while True:
        if len(lst) - token_len <= index_lst:
            break
        # found token?
        found_token = True
        for i in range(token_len):
            if lst[index_lst + i] != token[i]:
                found_token = False
        if found_token:
            token_count += 1
            index_lst += token_len
        else:
            index_lst += 1
    return token_count

def convert_moves_to_tokens(moves):
    tokens = {}
    tokens['A'] = []
    tokens['B'] = []
    tokens['C'] = []
    cur_token = 'A'
    cur_num_tokens = 0
    prev_num_tokens = 0
    index = 0
    while index < len(moves):
        # skip already replaced tokens
        if moves[index] == 'A' or moves[index] == 'B' or moves[index] == 'C':
            index += 1
            continue
        # Always at least two?
        if len(tokens[cur_token]) < 2:
            tokens[cur_token].append(moves[index])
        else:
            cur_num_tokens = count_tokens(moves, tokens[cur_token], cur_token)
            if cur_num_tokens >= prev_num_tokens:
                # we did good (probably)
                tokens[cur_token].append(moves[index])
                prev_num_tokens = cur_num_tokens
            else:
                if cur_token == 'C':
                    find_and_replace_token(moves, tokens[cur_token], cur_token)
                    break
                # need to pop one off and seal the deal
                tokens[cur_token].pop()
                index = find_and_replace_token(moves, tokens[cur_token], cur_token)
                cur_token = 'B' if cur_token == 'A' else 'C'
        index += 1
    return tokens

if __name__ == '__main__':
    # Part A
    print("Part A")
    int1 = IntCodeInterpreter(getMemory(), [])
    gen = int1.runIntCode()

    the_map = []
    row = []
    for output in gen:
        the_char = chr(output)
        if the_char == '\n':
            the_map.append(copy.deepcopy(row))
            row.clear()
        else:
            row.append(the_char)
    #for output_row in the_map:
    #    print("".join(output_row), sep="")

    i_coords = find_intersection_coords(the_map)
    sum = 0
    for coord in i_coords:
        sum += coord[0] * coord[1]
    print(sum)

    # Part B
    print("Part B")
    # robot coord?
    cur_pos = find_robot_coord(the_map)
    droid = Droid(cur_pos[0], cur_pos[1], the_map)

    moves = []
    while True:
        move = droid.get_next_set_of_moves()
        if move == None:
            break
        else:
            moves.append(move)
            droid.do_move(move)
    
    tokens = convert_moves_to_tokens(moves)

    droid.input_main_seq(moves)
    droid.input_function(tokens['A'])
    droid.input_function(tokens['B'])
    droid.input_function(tokens['C'])

    droid.brain.inputs.append(ord('n'))
    droid.brain.inputs.append(ord('\n'))
    
    last_output = 0
    for output in droid.brain.runIntCode():
        last_output = output
    print(last_output)