from collections import defaultdict
from collections import deque
import math
import copy

def build_sensible_map():
    f = open("input18.txt", 'r')
    content = f.read()
    f.close()
    lines = content.split('\n')
    the_map = [list(x) for x in lines]
    sensible_map = defaultdict(lambda:'#')
    for y in range(len(the_map)):
        for x in range(len(the_map[y])):
            if the_map[y][x] != '#':
                sensible_map[(x, y)] = the_map[y][x]
    return sensible_map

def find_starting_positions(sensible_map):
    poses = []
    for coord, key in sensible_map.items():
        if key == '@':
            poses.append(coord)
    return poses

def get_interesting_neighbors(sensible_map, coord, remaining_keys):
    x = coord[0]
    y = coord[1]
    ns = []
    coords = [(x, y + 1), (x, y - 1), (x + 1, y), (x - 1, y)]
    for coord in coords:
        n = sensible_map[coord]
        if n.islower() or n == '.' or n == '@':
            ns.append(coord)
        if n.isupper() and not n.lower() in remaining_keys:
            ns.append(coord)
    return ns

def find_keys(sensible_map):
    keys = []
    for val in sensible_map.values():
        if val.islower():
            keys.append(val)
    return keys

def calc_cost_with_flood_fill(sensible_map, start_coord, remaining_keys):
    costs = []
    the_queue = deque()
    discovered = set()
    discovered.add(start_coord)
    the_queue.append(start_coord)
    cost = 0
    while the_queue:
        lst = []
        while the_queue:
            lst.append(the_queue.pop())
        for cur_pos in lst:
            key = sensible_map[cur_pos]
            if key.islower() and key in remaining_keys:
                tup = (cur_pos, cost, key)
                costs.append(tup)
            for n in get_interesting_neighbors(sensible_map, cur_pos, remaining_keys):
                if not n in discovered:
                    the_queue.append(n)
                    discovered.add(n)
        cost += 1
    return costs

def update_min_cost_lib(remaining_keys, start_coord, min_cost_library, val):
    lel = "".join(remaining_keys)
    key = (lel, start_coord)
    min_cost_library[key] = val

def get_min_cost_remaining_path(remaining_keys, start_coord, min_cost_library):
    lel = "".join(remaining_keys)
    key = (lel, start_coord)
    min_cost = min_cost_library.get(key, None)
    return min_cost

def find_shortest_r(sensible_map, remaining_keys, start_coord, min_cost_library, cur_cost, cur_min_value):
    if cur_cost >= cur_min_value[0]:
        return math.inf
    if len(remaining_keys) == 0:
        if cur_cost < cur_min_value[0]:
            print(cur_cost)
            cur_min_value[0] = cur_cost
        return 0
    min_cost = get_min_cost_remaining_path(remaining_keys, start_coord, min_cost_library)
    if min_cost != None:
        return min_cost
    min_cost = math.inf
    avail_paths = calc_cost_with_flood_fill(sensible_map, start_coord, remaining_keys)
    #print(f"from coord {start_coord} found avail paths {avail_paths}")
    for cur_pos, cost, key in avail_paths:
        new_keys = copy.deepcopy(remaining_keys)
        new_keys.remove(key)
        new_keys = sorted(new_keys)
        val = cost + find_shortest_r(sensible_map, new_keys, cur_pos, min_cost_library, cur_cost + cost, cur_min_value)
        update_min_cost_lib(new_keys, cur_pos, min_cost_library, val - cost)
        min_cost = min(min_cost, val)
    return min_cost

def find_shortest_r_part_b(sensible_map, remaining_keys, cur_poses, min_cost_library, cur_cost, cur_min_value, cur_quadrant, tried_quads):
    #print("remaining keys", remaining_keys)
    start_coord = cur_poses[cur_quadrant]
    #print("looking from coord: ", start_coord)
    if cur_cost >= cur_min_value[0]:
        return math.inf
    if len(remaining_keys) == 0:
        if cur_cost < cur_min_value[0]:
            #print(cur_cost)
            cur_min_value[0] = cur_cost
        return 0
    min_cost = get_min_cost_remaining_path(remaining_keys, start_coord, min_cost_library)
    if min_cost != None:
        return min_cost
    min_cost = math.inf
    avail_paths = calc_cost_with_flood_fill(sensible_map, start_coord, remaining_keys)
    if len(avail_paths) == 0:
        if len(tried_quads) == 4:
            # Will recurse to madness
            print("tried too many quads")
            return math.inf
        #print("no avail paths in quadrant: ", cur_quadrant)
        # time to switch quadrant
        #min_cost_quad = math.inf
        #sum_cost_quads = 0
        #for new_quadrant in range(3):
            # Try all quadrants
            #cost = find_shortest_r_part_b(sensible_map, remaining_keys, cur_poses, min_cost_library, cur_cost, cur_min_value, (cur_quadrant + 1) % 4)
        #print("branching to quad ", (cur_quadrant + 1) % 4)
        new_tried_quads = copy.deepcopy(tried_quads)
        new_tried_quads.append(cur_quadrant)
        return find_shortest_r_part_b(sensible_map, remaining_keys, cur_poses, min_cost_library, cur_cost, cur_min_value, (cur_quadrant + 1) % 4, new_tried_quads)
        #print("it returned ", cost)
            #min_cost_quad = min(cost, min_cost_quad)
        #return sum_cost_quads
    else:
        #print(f"from coord {start_coord} found avail paths {avail_paths}")
        tried_quads.clear()
        for cur_pos, cost, key in avail_paths:
            #print("consumed key: ", key)
            new_cur_poses = copy.deepcopy(cur_poses)
            new_cur_poses[cur_quadrant] = cur_pos
            new_keys = copy.deepcopy(remaining_keys)
            new_keys.remove(key)
            new_keys = sorted(new_keys)
            val = cost + find_shortest_r_part_b(sensible_map, new_keys, new_cur_poses, min_cost_library, cur_cost + cost, cur_min_value, cur_quadrant, tried_quads)
            update_min_cost_lib(new_keys, cur_pos, min_cost_library, val - cost)
            min_cost = min(min_cost, val)
        return min_cost

def update_map(sensible_map):
    starting_pos = find_starting_positions(sensible_map)[0]
    x = starting_pos[0]
    y = starting_pos[1]
    for x_i in range(x - 1, x + 2):
        for y_i in range(y - 1, y + 2):
            if (x_i == x - 1 and y_i == y - 1) or (x_i == x - 1 and y_i == y + 1) or (x_i == x + 1 and y_i == y - 1) or (x_i == x + 1 and y_i == y + 1):
                sensible_map[(x_i, y_i)] = '@'
            else:
                sensible_map[(x_i, y_i)] = '#'

if __name__ == '__main__':
    sensible_map = build_sensible_map()
    starting_pos = find_starting_positions(sensible_map)[0]
    print("starting pos: ", starting_pos)
    min_cost_lib = {}
    cur_min_value = [math.inf]
    cur_cost = 0
    #print(sensible_map[(1,1)])
    #print(get_interesting_neighbors(sensible_map, (1,1)))
    remaining_keys = find_keys(sensible_map)
    print("remaining keys: ", remaining_keys)
    #print(calc_cost_with_flood_fill(sensible_map, (8, 2), remaining_keys))

    #minimal = find_shortest_r(sensible_map, remaining_keys, starting_pos, min_cost_lib, cur_cost, cur_min_value)
    #print(minimal)

    update_map(sensible_map)
    starting_poses = find_starting_positions(sensible_map)
    print(starting_poses)
    min_val = find_shortest_r_part_b(sensible_map, remaining_keys, starting_poses, min_cost_lib, cur_cost, cur_min_value, 0, [])
    print(min_val)
    