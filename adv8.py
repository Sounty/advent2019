import unittest

def get_pixel_stream():
    f = open("input8.txt", 'r')
    content = f.read()
    f.close()
    return content

def pretty_print_image(image):
    for y in range(len(image)):
        print(*image[y], sep=',')

def build_layers(width, height, pixel_stream):
    layers = []
    pixel_pos = 0
    while len(pixel_stream) > pixel_pos:
        image = [[0 for x in range(width)] for y in range(height)]
        for y in range(height):
            for x in range(width):
                image[y][x] = int(pixel_stream[pixel_pos])
                pixel_pos += 1
        layers.append(image)
    return layers

def find_layer_with_fewest_zeros(layers):
    min_num_zeros = 999999999999
    interesting_layer = 0
    for layer in range(len(layers)):
        num_zeros = sum_pixel_type_in_layer(layers[layer], 0)
        if num_zeros < min_num_zeros:
            interesting_layer = layer
            min_num_zeros = num_zeros
    return interesting_layer

def sum_pixel_type_in_layer(layer, pixel_type):
    sum_pixel = 0
    for y in range(len(layer)):           
        for x in range(len(layer[0])):
            if layer[y][x] == pixel_type:
                sum_pixel += 1
    return sum_pixel

def multiply_num_pixels_of_types(layer, pixel_type_1, pixel_type_2):
    return sum_pixel_type_in_layer(layer, pixel_type_1) * sum_pixel_type_in_layer(layer, pixel_type_2)

def calc_final_image(layers, width, height):
    # Create transparent image
    image = [[2 for x in range(width)] for y in range(height)]
    for layer in range(len(layers)):
        for y in range(len(layers[0])):
            for x in range(len(layers[0][0])):
                color = layers[layer][y][x]
                if image[y][x] == 2 and color != 2:
                    image[y][x] = color
    return image


class TestImagePass(unittest.TestCase):
    def test_small_program(self):
        layers = build_layers(3, 2, "123456789012")
        self.assertEqual(find_layer_with_fewest_zeros(layers), 0)
        self.assertEqual(multiply_num_pixels_of_types(layers[0], 1, 2), 1)
        pretty_print_image(layers[0])

    def test_part_a(self):
        pixel_stream = get_pixel_stream()
        layers = build_layers(25, 6, pixel_stream)
        layer = find_layer_with_fewest_zeros(layers)
        self.assertEqual(layer, 5)
        self.assertEqual(multiply_num_pixels_of_types(layers[layer], 1, 2), 2460)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestImagePass)
    unittest.TextTestRunner(verbosity=2).run(suite)

    # part b
    pixel_stream = get_pixel_stream()
    width = 25
    height = 6
    layers = build_layers(width, height, pixel_stream)
    final_image = calc_final_image(layers, width, height)
    pretty_print_image(final_image)