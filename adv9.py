import unittest
import copy

def getMemory():
    f = open("input9.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

def parse_op_code_with_param_modes(number):
    param_modes = []
    op_code = number % 100
    number = int(number / 100)
    num_used_param_modes = 1
    if op_code == 5 or op_code == 6:
        num_used_param_modes += 1
    elif op_code == 1 or op_code == 2 or op_code == 7 or op_code == 8:
        num_used_param_modes += 2
    for i in range(num_used_param_modes):
        param_modes.append(number % 10)
        number = int(number / 10)
    return (op_code, param_modes)

def add(value1, value2):
    return value1 + value2
def mult(value1, value2):
    return value1 * value2
def eq(value1, value2):
    if value1 == value2:
        return 1
    return 0
def lt(value1, value2):
    if value1 < value2:
        return 1
    return 0


class IntCodeInterpreter:
    def __init__(self, memory, inputs):
        self.memory = copy.deepcopy(memory)
        self.pc = 0
        self.inputs = inputs
        self.is_halted = False
        self.rest_mem = {}
        self.relative_addr = 0
        self.debug = False

    def write(self, pos, value, param_mode):
        final_pos = pos
        if param_mode == 2:
            final_pos += self.relative_addr
        if len(self.memory) > final_pos:
            # Regular write
            self.memory[final_pos] = value
        else:
            # Write to alt. memory
            self.rest_mem[final_pos] = value

    def read(self, pos, param_mode):
        final_pos = pos
        if param_mode == 2:
            final_pos += self.relative_addr
        if len(self.memory) > final_pos:
            # Regular read
            return self.memory[final_pos]
        else:
            # Read from alt. memory
            return self.rest_mem.get(final_pos, 0)

    def get_parameters(self, param_modes, params, amount):
        values = []
        for i in range(0, amount):
            pos_or_val = params[i]
            if param_modes[i] == 1:
                values.append(pos_or_val)
            else:
                values.append(self.read(pos_or_val, param_modes[i]))
        return values

    def add_or_mult(self, param_modes, params, func):
        values = self.get_parameters(param_modes, params, 2)
        pos = params[2]
        self.write(pos, func(values[0], values[1]), param_modes[2])
        self.pc += 4

    def jump_if_true(self, param_modes, params):
        values = self.get_parameters(param_modes, params, 2)
        if values[0] == 0:
            self.pc += 3
        else:
            self.pc = values[1]

    def jump_if_false(self, param_modes, params):
        values = self.get_parameters(param_modes, params, 2)
        if values[0] != 0:
            self.pc += 3
        else:
            self.pc = values[1]

    def my_cmp(self, param_modes, params, cmp_func):
        values = self.get_parameters(param_modes, params, 2)
        pos = params[2]
        self.write(pos, cmp_func(values[0], values[1]), param_modes[2])
        self.pc += 4

    def handle_output(self, param_modes):
        posOrValue = self.memory[self.pc + 1]
        self.pc += 2
        if param_modes[0] == 1:
            return posOrValue
        else:
            return self.read(posOrValue, param_modes[0])

    def handle_input(self, param_modes):
        if (len(self.inputs) == 0):
            # Now we need to wait for output from another module
            return False
        pos = self.memory[self.pc + 1]
        self.write(pos, self.inputs.pop(0), param_modes[0])
        self.pc += 2
        return True

    def handle_rel_input(self, param_modes, params):
        values = self.get_parameters(param_modes, params, 1)
        self.relative_addr += values[0]
        self.pc += 2

    def runIntCode(self):
        while True:
            op_code, param_modes = parse_op_code_with_param_modes(self.memory[self.pc])
            if op_code == 99:
                self.is_halted = True
                break
            if op_code == 1:
                if self.debug:
                    print("ADD")
                self.add_or_mult(param_modes, self.memory[self.pc + 1: self.pc + 4], add)
            elif op_code == 2:
                if self.debug:
                    print("MULT")
                self.add_or_mult(param_modes, self.memory[self.pc + 1: self.pc + 4], mult)
            elif op_code == 3:
                if self.debug:
                    print("INPUT")
                self.handle_input(param_modes)
            elif op_code == 4:
                if self.debug:
                    print("OUTPUT")
                yield self.handle_output(param_modes)
            elif op_code == 5:
                if self.debug:
                    print("JUMP TRUE")
                self.jump_if_true(param_modes, self.memory[self.pc + 1: self.pc + 3])
            elif op_code == 6:
                if self.debug:
                    print("JUMP FALSE")
                self.jump_if_false(param_modes, self.memory[self.pc + 1: self.pc + 3])
            elif op_code == 7:
                if self.debug:
                    print("LESS THAN")
                self.my_cmp(param_modes, self.memory[self.pc + 1: self.pc + 4], lt)
            elif op_code == 8:
                if self.debug:
                    print("EQUAL")
                self.my_cmp(param_modes, self.memory[self.pc + 1: self.pc + 4], eq)
            elif op_code == 9:
                if self.debug:
                    print("RELATIVE OUTPUT")
                self.handle_rel_input(param_modes, self.memory[self.pc + 1: self.pc + 2])
            else:
                print("Unknown op_code: ", op_code)

class TestIntCode(unittest.TestCase):
    def test_small_program(self):
        prog1 = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]
        int1 = IntCodeInterpreter(prog1, [])
        outputs = []
        gen = int1.runIntCode()
        for x in gen:
            outputs.append(x)
        self.assertEqual(prog1, outputs)

    def test_large_middle_num(self):
        prog = [104,1125899906842624,99]
        int1 = IntCodeInterpreter(prog, [])
        self.assertEqual(prog[1], next(int1.runIntCode()))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestIntCode)
    unittest.TextTestRunner(verbosity=2).run(suite)

    #part a
    prog = getMemory()
    int1 = IntCodeInterpreter(prog, [1])
    print("part_a: ", next(int1.runIntCode()))

    int2 = IntCodeInterpreter(prog, [2])
    int2.runIntCode()
    print("part_b: ", next(int2.runIntCode()))