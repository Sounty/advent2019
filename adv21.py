from adv9 import IntCodeInterpreter

def getMemory():
    f = open("input21.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

def write_instruction(droid, command_as_string):
    for c in command_as_string:
        droid.inputs.append(ord(c))
    droid.inputs.append(ord('\n'))

def print_output(droid):
    gen = droid.runIntCode()
    cur_row = []
    for output in gen:
        try:
            char = chr(output)
            if char == '\n':
                print("".join(cur_row), sep="")
                cur_row.clear()
            else:
                cur_row.append(char)
        except:
            print(output)

if __name__ == '__main__':
    droid = IntCodeInterpreter(getMemory(), [])

    print("PART A") # ABCD
    write_instruction(droid, "NOT C T") # store value 1 if hole in 3
    write_instruction(droid, "OR D J") # store value 1 if ground in 4
    write_instruction(droid, "AND T J") # then we need to jump now

    write_instruction(droid, "NOT A T") # need to jump right now
    write_instruction(droid, "OR T J")
    write_instruction(droid, "WALK")

    print_output(droid)
    

    running_droid = IntCodeInterpreter(getMemory(), [])
    print("PART B")
    write_instruction(running_droid, "NOT C T")
    write_instruction(running_droid, "AND D T")
    write_instruction(running_droid, "OR T J")

    write_instruction(running_droid, "AND E T")
    write_instruction(running_droid, "OR H T")
    write_instruction(running_droid, "AND T J")

    write_instruction(running_droid, "NOT B T")
    write_instruction(running_droid, "AND C T")
    write_instruction(running_droid, "AND D T")
    write_instruction(running_droid, "OR T J")

    write_instruction(running_droid, "NOT A T") # need to jump right now
    write_instruction(running_droid, "OR T J")
    write_instruction(running_droid, "RUN")

    print_output(running_droid)