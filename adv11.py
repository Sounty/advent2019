import unittest
from adv9 import IntCodeInterpreter

def getMemory():
    f = open("input11.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

class PainterRobot:
    def __init__(self):
        self.tile_paints = {} # (x, y) as key , paint as value
        self.brain = IntCodeInterpreter(getMemory(), [])
        self.x = 0
        self.y = 0
        self.facing = '^'
        self.painted_tiles = set()
        self.x_min = 0
        self.y_min = 0
        self.x_max = 0
        self.y_max = 0

    def change_dir(self, direction):
        if self.facing == '^':
            self.facing = '<' if direction == 0 else '>'
        elif self.facing == '<':
            self.facing = 'v' if direction == 0 else '^'
        elif self.facing == 'v':
            self.facing = '>' if direction == 0 else '<'
        elif self.facing == '>':
            self.facing = '^' if direction == 0 else 'v'

    def move(self):
        if self.facing == '^':
            self.y += 1
            if self.y > self.y_max:
                self.y_max = self.y
        elif self.facing == '<':
            self.x -= 1
            if self.x < self.x_min:
                self.x_min = self.x
        elif self.facing == 'v':
            self.y -= 1
            if self.y < self.y_min:
                self.y_min = self.y
        elif self.facing == '>':
            self.x += 1
            if self.x > self.x_max:
                self.x_max = self.x

    def paint_over(self, color):
        self.tile_paints[(self.x, self.y)] = color

    def run(self):
        input = self.tile_paints.get((self.x, self.y), 0)
        self.brain.inputs.append(input)
        gen = self.brain.runIntCode()
        for paint_to_color in gen:
            if paint_to_color != input:
                # it paints!
                self.painted_tiles.add((self.x, self.y))
            self.paint_over(paint_to_color)

            direction = next(gen)
            self.change_dir(direction)
            self.move()
            input = self.tile_paints.get((self.x, self.y), 0)
            self.brain.inputs.append(input)

    def print_board(self):
        for y in reversed(range(self.y_min, self.y_max+1)):
            row = []
            for x in range(self.x_min, self.x_max+1):
                color = self.tile_paints.get((x, y), 0)
                if color == 1:
                    row.append('##')
                else:
                    row.append('  ')
            print("".join(row), sep='')

if __name__ == '__main__':
    # part a
    robot = PainterRobot()
    robot.run()
    print(len(robot.painted_tiles))
    
    # part b
    robot_b = PainterRobot()
    robot_b.tile_paints[(0, 0)] = 1
    robot_b.run()
    robot_b.print_board()