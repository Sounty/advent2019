from adv9 import IntCodeInterpreter
import time


def getMemory():
    f = open("input13.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

def count_blocks(screen):
    counter = 0
    for key, obj in screen.items():
        if obj == 2:
            counter += 1
    return counter

def print_screen(screen, x_max, y_max, player_score):
    #print(f"PLAYER SCORE: {player_score}")
    for y in range(y_max + 1):
        row = []
        for x in range(x_max + 1):
            obj = screen.get((x, y), 0)
            if obj == 0:
                row.append(' ')
            if obj == 1:
                row.append('#')
            if obj == 2:
                row.append('x')
            if obj == 3:
                row.append('_')
            if obj == 4:
                row.append('o')
        print("".join(row), sep='')

if __name__ == '__main__':
    print("PART A")
    screen = {}
    mem = getMemory()
    int1 = IntCodeInterpreter(mem, [])
    gen = int1.runIntCode()
    x_max = 0
    y_max = 0
    for x in gen:
        y = next(gen)
        if x > x_max:
            x_max = x
        if y > y_max:
            y_max = y
        obj = next(gen)
        screen[(x, y)] = obj
    print(count_blocks(screen))

    print("PART B")
    mem[0] = 2
    int2 = IntCodeInterpreter(mem, [])
    gen = int2.runIntCode()
    
    joystick = 0
    int2.inputs.append(joystick)
    ball_pos_x = 0
    x_paddle_pos = 0
    player_score = 0
    for x in gen:
        y = next(gen)
        obj = next(gen)
        if x == -1 and y == 0:
            player_score = obj
            continue
        #screen[(x, y)] = obj
        if obj == 4:
            ball_pos_x = x
            #print_screen(screen, x_max, y_max, player_score)
            #time.sleep(0.01)
        if obj == 3:
            x_paddle_pos = x
            #print_screen(screen, x_max, y_max, player_score)
            #time.sleep(0.01)
        if x_paddle_pos > ball_pos_x:
            # go left
            joystick = -1
        elif x_paddle_pos < ball_pos_x:
            joystick = 1
        else:
            joystick = 0

        # Read joystick
        int2.inputs.clear()
        int2.inputs.append(joystick)
    print("Final score: ", player_score)
            
        

        
        



    
