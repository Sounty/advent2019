import sys
import unittest

def getContent(noun, verb, pathToFile):
    f = open(pathToFile, 'r')
    content = f.read().split(',')
    f.close()
    content[1] = noun
    content[2] = verb
    return content

def runIntCode(content):
    pc = 0
    while True:
        opCode = int(content[pc])
        if opCode == 99:
            break
        pos1 = int(content[pc + 1])
        pos2 = int(content[pc + 2])
        resPos = int(content[pc + 3])
        if opCode == 1:
            content[resPos] = int(content[pos1]) + int(content[pos2])
        elif opCode == 2:
            content[resPos] = int(content[pos1]) * int(content[pos2])
        pc += 4
    return content

def part_a():
    pathToFile = "input2.txt"
    noun = 12
    verb = 2
    content = getContent(noun, verb, pathToFile)
    content = runIntCode(content)
    return content[0]

def part_b():
    pathToFile = "input2.txt"
    for noun in range(100):
        for verb in range(100):
            content = getContent(noun, verb, pathToFile)
            if runIntCode(content)[0] == 19690720:
                return 100*noun + verb

class TestIntCode(unittest.TestCase):
    def test_small_programs(self):
        self.assertEqual(runIntCode([1,0,0,0,99]), [2,0,0,0,99])
        self.assertEqual(runIntCode([2,3,0,3,99]), [2,3,0,6,99])
        self.assertEqual(runIntCode([2,4,4,5,99,0]), [2,4,4,5,99,9801])
        self.assertEqual(runIntCode([1,1,1,4,99,5,6,0,99]), [30,1,1,4,2,5,6,0,99])

    def test_part_a(self):
        self.assertEqual(part_a(), 9706670)

    def test_part_b(self):
        self.assertEqual(part_b(), 2552)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestIntCode)
    unittest.TextTestRunner(verbosity=2).run(suite)