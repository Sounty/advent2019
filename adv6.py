import unittest
import copy

class Node:
    def __init__(self, name):
        self.name = name
        self.children = []

def get_inputs_from_file():
    pathToFile = "input6.txt"
    f = open(pathToFile, 'r')
    lines = f.readlines()
    return lines

def get_nodes(line):
    return line.strip(' \n\t').split(')')

def get_inputs(map_as_string):
    return map_as_string.split('\n')

def create_children_of_node(node, input_list):
    for rel in input_list:
        nodes = get_nodes(rel)
        if nodes[0] == node.name:
            child = Node(nodes[1])
            node.children.append(child)

def find_root_node(input_list):
    parent_nodes = set()
    child_nodes = set()
    for rel in input_list:
        nodes = get_nodes(rel)
        parent_nodes.add(nodes[0])
        child_nodes.add(nodes[1])
    a = parent_nodes - child_nodes
    if len(a) != 1:
        print("fucked up when trying to find root")
    return a.pop()

def build_tree_helper(input_list, root):
    create_children_of_node(root, input_list)
    for child in root.children:
        build_tree_helper(input_list, child)

def build_tree(input_list):
    root = Node(find_root_node(input_list))
    build_tree_helper(input_list, root)
    return root

def print_tree(root):
    print(root.name)
    for child in root.children:
        print_tree(child)

def calc_sum_depth(root, depth, total):
    total = total + depth
    for child in root.children:
        total = calc_sum_depth(child, depth + 1, total)
    return total

def get_path(root, path, name):
    if root.name == name:
        path.add(root.name)
        return True
    for child in root.children:
        if get_path(child, path, name):
            path.add(child.name)
            return True
    return False
    

def count_orbits(input_list):
    # Build tree
    root = build_tree(input_list)
    depth = calc_sum_depth(root, 0, 0)
    return depth

def count_transfers(input_list):
    root = build_tree(input_list)
    you_path = set()
    san_path = set()
    get_path(root, you_path, "YOU")
    get_path(root, san_path, "SAN")
    common_path = you_path.intersection(san_path)
    return len(you_path - common_path) + len(san_path - common_path) - 2

class TestOrbitCounter(unittest.TestCase):
    def test_find_root_node(self):
        input_map = '''COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L'''
        inputs = get_inputs(input_map)
        self.assertEqual(find_root_node(inputs), "COM")

    def test_small_program(self):
        input_map = '''COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L'''
        inputs = get_inputs(input_map)
        self.assertEqual(count_orbits(inputs), 42)

    def test_small_program_b(self):
        input_map = '''COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN'''
        inputs = get_inputs(input_map)
        self.assertEqual(count_transfers(inputs), 4)
        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestOrbitCounter)
    unittest.TextTestRunner(verbosity=2).run(suite)

    inputs = get_inputs_from_file()
    # Run part a
    print("Answer part a: ", count_orbits(inputs))
    print("Answer part b: ", count_transfers(inputs))