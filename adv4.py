import unittest

#pass_code is a string
def is_valid_pass(pass_code):
    found_double_number = False
    for pos in range(5):
        curNum = int(pass_code[pos])
        nextNum = int(pass_code[pos + 1])
        if curNum > nextNum:
            return False
        if curNum == nextNum:
            found_double_number = True
    return found_double_number

def is_valid_pass_part_b(pass_code):
    found_adjecent_numbers = 0
    found_double_number = False
    counting = False
    for pos in range(5):
        curNum = int(pass_code[pos])
        nextNum = int(pass_code[pos + 1])
        if curNum > nextNum:
            return False
        if curNum == nextNum:
            if not counting:
                found_adjecent_numbers += 2
                counting = True
            else:
                found_adjecent_numbers += 1
        else:
            if found_adjecent_numbers == 2:
                found_double_number = True
            found_adjecent_numbers = 0
            counting = False
    return found_adjecent_numbers == 2 or found_double_number

def find_valid_passwords(lower_bound, upper_bound, passValidator):
    valid_passwords = 0
    for pass_code in range(lower_bound, upper_bound + 1):
        if passValidator(str(pass_code)):
            valid_passwords += 1
    return valid_passwords


class TestPasswordFinder(unittest.TestCase):
    def test_valid_password(self):
        self.assertEqual(is_valid_pass("111111"), True)
        self.assertEqual(is_valid_pass("223450"), False)
        self.assertEqual(is_valid_pass("123789"), False)

    def test_valid_password_part_b(self):
        self.assertEqual(is_valid_pass_part_b("112233"), True)
        self.assertEqual(is_valid_pass_part_b("123444"), False)
        self.assertEqual(is_valid_pass_part_b("111122"), True)

    def test_brute_force(self):
        self.assertEqual(find_valid_passwords(206938, 679128, is_valid_pass), 1653)

    def test_brute_force_part_b(self):
        self.assertEqual(find_valid_passwords(206938, 679128, is_valid_pass_part_b), 1133)
        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPasswordFinder)
    unittest.TextTestRunner(verbosity=2).run(suite)