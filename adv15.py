from adv9 import IntCodeInterpreter
import time
from collections import deque
from collections import defaultdict
from math import inf

def getMemory():
    f = open("input15.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

NORTH = 1
SOUTH = 2
WEST = 3
EAST = 4

class OxyDroid:
    def __init__(self):
        mem = getMemory()
        self.brain = IntCodeInterpreter(mem, [])
        self.x_pos = 0
        self.y_pos = 0
        self.x_max = 0
        self.x_min = 0
        self.y_max = 0
        self.y_min = 0
        self.visited = set()
        self.moves = []
        self.map = {}

    def get_next_loc(self, dir):
        if dir == NORTH:
            return (self.x_pos, self.y_pos + 1)
        elif dir == SOUTH:
            return (self.x_pos, self.y_pos - 1)
        elif dir == WEST:
            return (self.x_pos - 1, self.y_pos)
        elif dir == EAST:
            return (self.x_pos + 1, self.y_pos)

    def has_visited_dir(self, dir):
        pos = self.get_next_loc(dir)
        if pos in self.visited:
            return True
        return False

    def print_map(self):
        print("=======================================")
        for y in range(self.y_min - 1, self.y_max + 2):
            row = []
            for x in range(self.x_min - 1, self.x_max + 2):
                tile = self.map.get((x, y), ' ')
                if self.x_pos == x and self.y_pos == y:
                    tile = 'D'
                elif x == 0 and y == 0:
                    tile = 'S'
                row.append(tile)
            print("".join(row), sep="")
        print("=======================================")

    def update(self, coord, res):
        self.visited.add(coord)
        if res == 0:
            self.map[coord] = '#'
            return
        elif res == 1:
            self.map[coord] = '.'
        else:
            self.map[coord] = 'O'
        self.x_pos = coord[0]
        self.y_pos = coord[1]
        self.moves.append((self.x_pos, self.y_pos))
        self.x_max = self.x_pos if self.x_pos > self.x_max else self.x_max
        self.y_max = self.y_pos if self.y_pos > self.y_max else self.y_max
        self.x_min = self.x_pos if self.x_pos < self.x_min else self.x_min
        self.y_min = self.y_pos if self.y_pos < self.y_min else self.y_min
    
    def move_one_step(self, dir):
        next_pos = self.get_next_loc(dir)
        self.brain.inputs.append(dir)
        res = next(self.brain.runIntCode())
        self.update(next_pos, res)
        return res

    def backtrack_one_step(self):
        cur_pos = self.moves.pop() # current pos
        move_to_pos = self.moves.pop() # prev pos
        if move_to_pos == (0, 0):
            print("moving back to start")
            self.print_map()
        if move_to_pos[0] > self.x_pos:
            self.move_one_step(EAST)
        elif move_to_pos[0] < self.x_pos:
            self.move_one_step(WEST)
        elif move_to_pos[1] > self.y_pos:
            self.move_one_step(NORTH)
        elif move_to_pos[1] < self.y_pos:
            self.move_one_step(SOUTH)

if __name__ == '__main__':
    droid = OxyDroid()
    oxy_coord = None
    while True:
        # Try all directions
        tried_all_dirs = True
        for dir in range(1, 5):
            if droid.has_visited_dir(dir):
                continue
            res = droid.move_one_step(dir)
            if res == 0:
                continue
            elif res == 1:
                tried_all_dirs = False
                break
            elif res == 2:
                # found oxygen
                oxy_coord = (droid.x_pos, droid.y_pos)
                print(f"oxygen coord: {oxy_coord} at {len(droid.moves)} steps from start")
        if tried_all_dirs:
            if len(droid.moves) == 1:
                droid.print_map()
                break
            # now we need to backtrack
            droid.backtrack_one_step()

    finished_map = droid.map
    coords_with_oxygen = deque()
    coords_with_oxygen.append(oxy_coord)
    minutes = 0
    while coords_with_oxygen:
        # get all neighbors of oxygen that can be filled
        all_stuff = []
        while coords_with_oxygen:
            all_stuff.append(coords_with_oxygen.pop())
        for coord in all_stuff:
            neighbors = []
            neighbors.append((coord[0] + 1, coord[1]))
            neighbors.append((coord[0] - 1, coord[1]))
            neighbors.append((coord[0], coord[1] + 1))
            neighbors.append((coord[0], coord[1] - 1))
            for n in neighbors:
                # fill in all neighbors
                if finished_map.get(n, '#') == '.':
                    finished_map[n] = 'O'
                    coords_with_oxygen.append(n)
        minutes += 1
    print(f"it took {minutes - 1} minutes to fill")

