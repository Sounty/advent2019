from adv9 import IntCodeInterpreter
from collections import deque
import threading
import queue
import time

def getMemory():
    f = open("input23.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

class DummyQueue:
    def __init__(self):
        self.internal = queue.Queue()

    def pop(self, index):
        if self.internal.qsize() == 0:
            return -1
        return self.internal.get()

    def append(self, elem):
        self.internal.put(elem)

    def is_empty(self):
        return self.internal.empty()

    def __len__(self):
        return 1
    

class Computer (threading.Thread):
    def __init__(self, network_address, packet_map, packet_map_lock, nat):
        threading.Thread.__init__(self, daemon=True)
        self.network_address = network_address
        packet_map[network_address].append(network_address)
        self.brain = IntCodeInterpreter(getMemory(), packet_map[network_address])
        self.gen = self.brain.runIntCode()
        self.packet_map = packet_map
        self.packet_map_lock = packet_map_lock
        self.nat = nat
        self.timepoint = time.time()

    def seconds_since_last_action(self):
        return time.time() - self.timepoint

    def run(self):
        # Send all packets
        for dest_addr in self.gen:
            x = next(self.gen)
            y = next(self.gen)
            if dest_addr == 255:
                self.nat.put((x, y))
            else:
                self.packet_map[dest_addr].append(x)
                self.packet_map[dest_addr].append(y)
            self.timepoint = time.time()

def init_packet_map(num_comps):
    packet_map = {}
    for i in range(num_comps):
        packet_map[i] = DummyQueue()
    return packet_map

if __name__ == '__main__':
    num_comps = 50
    last_nat = queue.LifoQueue()
    packet_map = init_packet_map(num_comps)
    computers_threads = {}
    for i in range(num_comps):
        thread =  Computer(i, packet_map, packet_map_lock, last_nat)
        computers_threads[i] = thread

    for comp in computers_threads.values():    
        comp.start()

    # Monitor all threads if they are idle. then restart(..) with last nat
    prev_last_y = 0
    while True:
        are_all_idle = True
        for key, comp in computers_threads.items():
            if comp.seconds_since_last_action() < 1: #10
                are_all_idle = False
                break
        if are_all_idle and not last_nat.empty():
            last = last_nat.get()
            if last[1] == prev_last_y:
                print(last)
                break
            packet_map[0].append(last[0])
            packet_map[0].append(last[1])
            # Now we don't want to spam with new inputs
            computers_threads[0].timepoint = time.time()
            prev_last_y = last[1]
