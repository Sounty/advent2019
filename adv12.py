import unittest
import re
import copy
import numpy

test_input = '''<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>'''

test_input = '''<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>'''

def build_moons(input):
    moons = []
    lines = input.split('\n')
    name = 0
    for line in lines:
        digits = re.findall(r'-?\d+', line)
        moon = Moon(int(digits[0]), int(digits[1]), int(digits[2]), name)
        moons.append(moon)
        name += 1
    return moons

class Moon:
    def __init__(self, x, y, z, name):
        self.x = x
        self.y = y
        self.z = z
        self.vel_x = 0
        self.vel_y = 0
        self.vel_z = 0
        self.name = name

    def apply_velocity(self):
        self.x += self.vel_x
        self.y += self.vel_y
        self.z += self.vel_z

    def apply_gravity(self, other_moon):
        if self.x > other_moon.x:
            other_moon.vel_x += 1
            self.vel_x -= 1
        elif self.x < other_moon.x:
            other_moon.vel_x -= 1
            self.vel_x += 1
        if self.y > other_moon.y:
            other_moon.vel_y += 1
            self.vel_y -= 1
        elif self.y < other_moon.y:
            other_moon.vel_y -= 1
            self.vel_y += 1
        if self.z > other_moon.z:
            other_moon.vel_z += 1
            self.vel_z -= 1
        elif self.z < other_moon.z:
            other_moon.vel_z -= 1
            self.vel_z += 1

    def get_total_energy(self):
        potential_energy = abs(self.x) + abs(self.y) + abs(self.z)
        kinetic_energy = abs(self.vel_x) + abs(self.vel_y) + abs(self.vel_z)
        return potential_energy * kinetic_energy

    def print(self):
        print(f"NAME: {self.name} pos=<x= {self.x}, y= {self.y}, z= {self.z}>, vel=<x={self.vel_x}, y={self.vel_y}, z= {self.vel_z}>")

def sum_whole_system(moons):
    energy = 0
    for moon in moons:
        energy += moon.get_total_energy()
    return energy

def apply_step_to_universe(moons):
    index = 1
    # Apply gravity
    for moon1 in moons:
        for i in range(index, len(moons)):
            moon2 = moons[i]
            moon1.apply_gravity(moon2)
        index += 1
    # Apply velocity
    for moon in moons:
        moon.apply_velocity()

def get_cycle_time(moons):
    inits_x = []
    inits_y = []
    inits_z = []
    for moon in moons:
        inits_x.append(moon.x)
        inits_y.append(moon.y)
        inits_z.append(moon.z)
    step = 0
    x_cycle = 0
    y_cycle = 0
    z_cycle = 0
    while True:
        apply_step_to_universe(moons)
        step += 1
        has_all_reached_init_x = True
        has_all_reached_init_y = True
        has_all_reached_init_z = True
        for i in range(len(moons)):
            if moons[i].x != inits_x[i] or moons[i].vel_x != 0:
                has_all_reached_init_x = False
            if moons[i].y != inits_y[i] or moons[i].vel_y != 0:
                has_all_reached_init_y = False
            if moons[i].z != inits_z[i] or moons[i].vel_z != 0:
                has_all_reached_init_z = False
        if has_all_reached_init_x and x_cycle == 0:
            x_cycle = step
        if has_all_reached_init_y and y_cycle == 0:
            y_cycle = step
        if has_all_reached_init_z and z_cycle == 0:
            z_cycle = step

        if x_cycle != 0 and y_cycle != 0 and z_cycle != 0:
            break
    return [x_cycle, y_cycle, z_cycle]

if __name__ == '__main__':
    # Part a
    print("========PART A========")
    f = open("input12.txt", 'r')
    content = f.read()
    f.close()
    #content = test_input
    moons = build_moons(content)

    for i in range(1000):
        apply_step_to_universe(moons)
    print(sum_whole_system(moons))

    print("========PART B========")
    cycles = get_cycle_time(build_moons(content))
    print(numpy.lcm.reduce(cycles, dtype=numpy.uint64))
    


