import numpy

def get_adjecent_bugs(tiles, x, y):
    bugs = 0
    if x > 0:
        bugs += tiles[y][x - 1]
    if x < len(tiles[0]) - 1:
        bugs += tiles[y][x + 1]
    if y > 0:
        bugs += tiles[y - 1][x]
    if y < len(tiles) - 1:
        bugs += tiles[y + 1][x]
    return bugs

def step(tiles):
    next_step = numpy.zeros([5,5], dtype=int)
    for y in range(5):
        for x in range(5):
            cur_sign = tiles[y][x]
            adjecent_bugs = get_adjecent_bugs(tiles, x, y)
            if cur_sign == 0:
                # This is empty space.
                if adjecent_bugs == 1 or adjecent_bugs == 2:
                    # Gonna be infested
                    next_step[y][x] = 1
            else:
                # This is a bug
                if adjecent_bugs == 1:
                    next_step[y][x] = 1
    return next_step

def convert_to_tiles(rows):
    tiles = numpy.zeros([5,5], dtype=int)
    for x in range(5):
        for y in range(5):
            if rows[y][x] == '#':
                tiles[y][x] = 1
    return tiles

def print_tiles(tiles):
    print("")
    for y in range(5):
        row = []
        for x in range(5):
            if tiles[y][x] == 1:
                row.append("#")
            else:
                row.append('.')
        print("".join(row), sep="")
    print("")

def calc_biodiversity(tiles):
    exponent = 0
    sum_biodiv = 0
    for y in range(5):
        for x in range(5):
            if tiles[y][x] == 1:
                sum_biodiv += pow(2, exponent)
            exponent += 1
    return sum_biodiv

if __name__ == '__main__':
    f = open("input24.txt", 'r')
    rows = f.read().split('\n')
    f.close()

    tiles = convert_to_tiles(rows)
    print_tiles(tiles)

    biodivs = set()
    while True:
        tiles = step(tiles)
        biodiv = calc_biodiversity(tiles)
        if biodiv in biodivs:
            print(biodiv)
            print_tiles(tiles)
            break
        biodivs.add(biodiv)
            
        


