import unittest
import copy
from itertools import permutations 

def getMemory():
    f = open("input7.txt", 'r')
    content = f.read().split(',')
    f.close()
    return [int(i) for i in content]

def parse_op_code_with_param_modes(number):
    param_modes = []
    op_code = -1
    while number:
        if op_code == -1:
            op_code = number % 100
            number = int(number / 100)
        divider = 10
        param_mode = number % divider
        param_modes.append(param_mode)
        number = int(number / divider)
    # Padd with extra 0's if needed
    while len(param_modes) != 3:
        param_modes.append(0)
    return (op_code, param_modes)

def add(value1, value2):
    return value1 + value2
def mult(value1, value2):
    return value1 * value2
def eq(value1, value2):
    if value1 == value2:
        return 1
    return 0
def lt(value1, value2):
    if value1 < value2:
        return 1
    return 0

def get_parameters(memory, param_modes, params, amount):
    values = []
    for i in range(0, amount):
        if param_modes[i] == 0:
            values.append(memory[params[i]])
        else:
            values.append(params[i])
    return values

class Amplifier:
    def __init__(self, memory, inputs):
        self.memory = copy.deepcopy(memory)
        self.pc = 0
        self.inputs = inputs
        self.outputs = []
        self.is_halted = False

    def add_or_mult(self, param_modes, params, func):
        values = get_parameters(self.memory, param_modes, params, 2)
        if param_modes[2] == 0:
            pos = params[2]
            self.memory[pos] = func(values[0], values[1])
        self.pc += 4

    def jump_if_true(self, param_modes, params):
        values = get_parameters(self.memory, param_modes, params, 2)
        if values[0] == 0:
            self.pc += 3
        else:
            self.pc = values[1]

    def jump_if_false(self, param_modes, params):
        values = get_parameters(self.memory, param_modes, params, 2)
        if values[0] != 0:
            self.pc += 3
        else:
            self.pc = values[1]

    def my_cmp(self, param_modes, params, cmp_func):
        values = get_parameters(self.memory, param_modes, params, 2)
        if param_modes[2] == 0:
            pos = params[2]
            self.memory[pos] = cmp_func(values[0], values[1])
        self.pc += 4

    def handle_output(self, param_modes):
        posOrValue = self.memory[self.pc + 1]
        if param_modes[0] == 0:
            self.outputs.append(self.memory[posOrValue])
        else:
            self.outputs.append(posOrValue)
        self.pc += 2

    def handle_input(self):
        if (len(self.inputs) == 0):
            # Now we need to wait for output from another module
            return False
        pos1 = self.memory[self.pc + 1]
        self.memory[pos1] = self.inputs.pop(0)
        self.pc += 2
        return True

    def runIntCode(self):
        while True:
            op_code, param_modes = parse_op_code_with_param_modes(self.memory[self.pc])
            if op_code == 99:
                self.is_halted = True
                break
            if op_code == 1:
                self.add_or_mult(param_modes, self.memory[self.pc + 1: self.pc + 4], add)
            elif op_code == 2:
                self.add_or_mult(param_modes, self.memory[self.pc + 1: self.pc + 4], mult)
            elif op_code == 3:
                if not self.handle_input():
                    break
            elif op_code == 4:
                self.handle_output(param_modes)
            elif op_code == 5:
                self.jump_if_true(param_modes, self.memory[self.pc + 1: self.pc + 3])
            elif op_code == 6:
                self.jump_if_false(param_modes, self.memory[self.pc + 1: self.pc + 3])
            elif op_code == 7:
                self.my_cmp(param_modes, self.memory[self.pc + 1: self.pc + 4], lt)
            elif op_code == 8:
                self.my_cmp(param_modes, self.memory[self.pc + 1: self.pc + 4], eq)

def runAmplCircuit(memory, phase_seq):
    max_thruster_output = 0
    # first iteration with phases
    ampls = []
    output_prev = 0
    for phase in phase_seq:
        ampl_i = Amplifier(memory, [phase, output_prev])
        ampl_i.runIntCode()
        output_prev = ampl_i.outputs.pop()
        ampls.append(ampl_i)
    # Are we done after the first iteration?
    if ampls[-1].is_halted:
        return output_prev

    while True:
        are_all_halted = True
        for ampl in ampls:
            if not ampl.is_halted:
                are_all_halted = False
        if are_all_halted:
            break
        # run one more time
        for i in range(5):
            cur_ampl = ampls[i]
            cur_ampl.inputs = [output_prev]
            cur_ampl.runIntCode()
            output_prev = cur_ampl.outputs.pop()
            if i == 4:
                if max_thruster_output < output_prev:
                    max_thruster_output = output_prev
    return max_thruster_output

def main(lower, upper):
    memory = getMemory()
    max_val = None
    seqs = list(permutations(range(lower, upper)))
    for seq in seqs:
        output = runAmplCircuit(memory, seq)
        if max_val == None or max_val < output:
            max_val = output
    return max_val

class TestAmplCircuit(unittest.TestCase):
    def test_small_program(self):
        self.assertEqual(runAmplCircuit([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], [4,3,2,1,0]), 43210)
        self.assertEqual(runAmplCircuit([3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0], [0,1,2,3,4]), 54321)
        self.assertEqual(runAmplCircuit([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0], [1,0,4,3,2]), 65210)

        # part b
        self.assertEqual(runAmplCircuit([3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5], [9,8,7,6,5]), 139629729)
        self.assertEqual(runAmplCircuit([3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10], [9,7,8,5,6]), 18216)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAmplCircuit)
    unittest.TextTestRunner(verbosity=2).run(suite)
    print("part_a: ", main(0, 5))
    print("part_b: ", main(5, 10))